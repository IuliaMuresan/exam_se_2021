import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileReader;

 class DisplayText extends  JFrame{
    JTextArea textArea1;
    JTextField textField1;
    JButton button1;
    JPanel panel;

    public DisplayText(){
        setSize(400,400);
        setLayout(new FlowLayout());


        panel=new JPanel();


        textArea1=new JTextArea();
        textArea1.setEditable(false);


        textField1=new JTextField(8);
        textField1.setEditable(true);

        button1=new JButton("Display");

        add(textArea1);
        add(textField1);
        add(button1);


        button1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                textArea1.setText(textField1.getText());
            }
        });
    }

    public static void main(String[] args) {
        DisplayText displayText=new DisplayText();
        displayText.setVisible(true);

    }
}