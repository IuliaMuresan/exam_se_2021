
interface I{}

class B extends G {
    long t;
    public int x() {
        return 0;
    }
}

class G {
    public int i() {
        return 0;
    }
}
class E implements I{
 public int metG(int i) {
    return i;
 }
}

class F {
    public int metA(){
        return 0;
    }
}
class H {

}

class D {

}
